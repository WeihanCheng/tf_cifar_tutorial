import tensorflow as tf
import numpy as np
from pre_processing import pre_process
import cv2


# Use PrettyTensor to simplify Neural Network construction.
import cifar10
cifar10.maybe_download_and_extract()
class_names = cifar10.load_class_names()

def _readData(sess):

    images_train, class_train, labels_train = cifar10.load_training_data()
    images_test, class_test, labels_test = cifar10.load_test_data()

    placeholders = [op for op in tf.get_default_graph().get_operations() if op.type == "Placeholder"]
    Distorted_Input = placeholders[3]._outputs[0]
    distorted_images = pre_process(images=Distorted_Input, training=False)
    distorted_images_test = sess.run(distorted_images, feed_dict={Distorted_Input: images_test})




    return images_train, distorted_images_test, labels_train, labels_test, class_train, class_test


def shuffle_data(sess, images, image_categories, images_classes, random_seed):

    np.random.seed(random_seed)
    np.random.shuffle(images)
    np.random.seed(random_seed)
    np.random.shuffle(image_categories)
    np.random.seed(random_seed)
    np.random.shuffle(images_classes)

    placeholders = [op for op in tf.get_default_graph().get_operations() if op.type == "Placeholder"]
    Distorted_Input = placeholders[3]._outputs[0]
    distorted_images = pre_process(images=Distorted_Input, training=True)
    distorted_images = sess.run(distorted_images, feed_dict={Distorted_Input: images})


    return distorted_images, image_categories, images_classes










