from argparse import ArgumentParser


TRAIN_PATH = ''
NUM_EPOCHES = 30000
BATCH_SIZE = 64  # or 128
CHECK_AFTER_STEPS = 300
LEARNING_RATE = 1e-3
NUMBER_categories = 10
SUB_DATASET_ENABLE = False

[IMAGE_WIDTH, IMAGE_HEIGHT] = [24, 24]

def argParser():
    parser = ArgumentParser()

    parser.add_argument('--train-path', type=str,
                        dest='train_path', help='path to training images folder',
                        metavar='TRAIN_PATH', default=TRAIN_PATH)
    parser.add_argument('--epoches', type=int,
                        dest='epoches', help='num epoches',
                        metavar='EPOCHES', default=NUM_EPOCHES)
    parser.add_argument('--batch-size', type=int,
                        dest='batch_size', help='batch size',
                        metavar='BATCH_SIZE', default=BATCH_SIZE)
    parser.add_argument('--check-after-steps', type=int,
                        dest='CHECK_AFTER_STEPS', help='CHECK_AFTER_STEPS',
                        metavar='CHECK_AFTER_STEPS',
                        default=CHECK_AFTER_STEPS)
    parser.add_argument('--learning-rate', type=float,
                        dest='learning_rate',
                        help='learning rate (default %(default)s)',
                        metavar='LEARNING_RATE', default=LEARNING_RATE)
    parser.add_argument('--training-width', type=int,
                        dest='cols',
                        help='image width',
                        metavar='TRAINING_WIDTH', default=IMAGE_WIDTH)
    parser.add_argument('--training-height', type=int,
                        dest='rows',
                        help='image height',
                        metavar='TRAINING_height', default=IMAGE_HEIGHT)
    parser.add_argument('--num_categories', type=int,
                        dest='num_categories',
                        help='num_categories',
                        metavar='num_categories', default=NUMBER_categories)
    parser.add_argument('--sub_dataset_enable', type=bool,
                        dest='sub_dataset_enable',
                        help='sub_dataset_enable',
                        metavar='sub_dataset_enable', default=SUB_DATASET_ENABLE)


    return parser


