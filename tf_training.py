import tensorflow as tf
import numpy as np
import time
import tf_readData as readData
import cv2
import os
import dl_model_deploy.TF_GraphConverter as converter


from cifar10 import img_size, num_channels


def _training(args, Outputlayer):

    OutputCategory = tf.placeholder(tf.float32, shape=[None, args.num_categories], name='Output_Category')
    global_step = tf.Variable(initial_value=0, name='global_step', trainable=False)
    LearningRate = tf.placeholder(tf.float32, name='Learning_Rate')


    placeholders = [op for op in tf.get_default_graph().get_operations() if op.type == "Placeholder"]
    Inputimage = placeholders[0]._outputs[0]

    # Evaluate and train
    # cross entropy of output as logits and labels
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=OutputCategory,
                                                                  logits=Outputlayer))  # calculate loss as cross entropy
    # train using Adamoptimizer
    optimizer = tf.train.AdamOptimizer(LearningRate).minimize(loss, global_step=global_step)

    correct_prediction = tf.equal(tf.argmax(Outputlayer, 1),
                                      tf.argmax(OutputCategory, 1))  # check if prediction was right
    inference_class = tf.argmax(Outputlayer, 1)
    # prediction accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))  # calculate accuracy

    # initialize variables
    sess = tf.InteractiveSession()  # a session for the graph execution
    sess.run(tf.global_variables_initializer())

    Distorted_Input = tf.placeholder(tf.float32, shape=[None, img_size, img_size, num_channels],
                       name='Distorted_Input')

    [images_train, images_test, image_categories_train, image_categories_test, class_train, class_test] = readData._readData(sess)

    t0 = time.time()
    # training iteration
    BATCHES_PER_EPOCH = int(len(images_train) / args.batch_size)

    # check accuracy times for every epoch
    CHECK_AFTER_STEPS = args.CHECK_AFTER_STEPS

    maxAccuracy = 0.0

    learning_rate = args.learning_rate
    sum_loss = 0.0
    loss_counter = 0

    for epoch in range(args.epoches):
        [shuffled_images_train, shuffled_image_categories_train, shuffled_class_train] = readData.shuffle_data(sess, images_train, image_categories_train, class_train, epoch)
        print("Epoch: %d" % (epoch))

        for i in range(BATCHES_PER_EPOCH):
            batch_im = shuffled_images_train[args.batch_size * i: args.batch_size * (i + 1)]
            batch_cat = shuffled_image_categories_train[args.batch_size * i: args.batch_size * (i + 1), :]
            batch_class = shuffled_class_train[args.batch_size * i: args.batch_size * (i + 1)]

            train_output = sess.run([loss, Outputlayer, OutputCategory, optimizer, global_step], feed_dict={Inputimage: batch_im, OutputCategory: batch_cat, LearningRate: learning_rate})
            # sum total loss for average loss
            sum_loss = sum_loss + train_output[0]
            loss_counter = loss_counter + 1

            # check accuracy by optimization steps
            if train_output[4] % CHECK_AFTER_STEPS == 0:
                avg_loss = sum_loss / loss_counter
                sum_loss = 0.0
                loss_counter = 0
                # testing accuracy using testing set
                accuracy_value = _testing(images_test, image_categories_test, class_test, inference_class)
                print('epoch: ', epoch, '   batch: ', i, '  avg loss: ', avg_loss, '    accuracy: ', float(accuracy_value), '   learning_rate', learning_rate)
                # each 50 epoch check max accuracy and save ckpt of max accuracy
                if accuracy_value >= maxAccuracy and epoch % 50 == 0:
                    maxAccuracy = accuracy_value
                    _saveCKPT(sess, Outputlayer, maxAccuracy, epoch)

        # 150 epoch decay learning rate
        if epoch % 150 == 0:
            # learning rate decay ratio = 0.9
            learning_rate = learning_rate * 0.9


    print('Time passed To end:', np.round(time.time() - t0), ' seconds')

    sess.close()  # close the session


def _testing(images, labels, cls_true, inference):

    batch_size = 256
    placeholders = [op for op in tf.get_default_graph().get_operations() if op.type == "Placeholder"]

    Inputimage = placeholders[0]._outputs[0]
    OutputCategory = placeholders[1]._outputs[0]

    # Number of images.
    num_images = len(images)

    # Allocate an array for the predicted classes which
    # will be calculated in batches and filled into this array.
    cls_pred = np.zeros(shape=num_images, dtype=np.int)

    # Now calculate the predicted classes for the batches.
    # We will just iterate through all the batches.
    # There might be a more clever and Pythonic way of doing this.

    # The starting index for the next batch is denoted i.
    i = 0

    while i < num_images:
        # The ending index for the next batch is denoted j.
        j = min(i + batch_size, num_images)

        # Create a feed-dict with the images and labels
        # between index i and j.
        feed_dict = {Inputimage: images[i:j, :],
                     OutputCategory: labels[i:j, :]}

        # Calculate the predicted class using TensorFlow.
        cls_pred[i:j] = inference.eval(feed_dict)
        # Set the start-index for the next batch to the
        # end-index of the current batch.
        i = j

    # Create a boolean array whether each image is correctly classified.
    correct = (cls_true == cls_pred)

    test_accuracy = correct.mean()

    return test_accuracy


def _saveCKPT(sess, Outputlayer, accuracy, epoch):
    saver = tf.train.Saver()
    CKPTPath = 'CKPT'
    if not os.path.exists(CKPTPath):
        os.makedirs(CKPTPath)
    CKPTName = 'model_' + str(round(accuracy, 3))
    if not os.path.exists(CKPTPath + '/' + CKPTName):
        os.makedirs(CKPTPath + '/' + CKPTName)
    saver.save(sess, CKPTPath + '/' + CKPTName + '/' + CKPTName + '_' + str(epoch) + '.ckpt')
    converter.Save_pb(sess, Outputlayer, CKPTPath + '/' + CKPTName + '/' + CKPTName + '_' + str(epoch))



