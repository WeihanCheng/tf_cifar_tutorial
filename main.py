import tensorflow as tf
import numpy as np
import os
import tf_training as training
from tf_argParser import argParser
import dl_model_deploy.TF_GraphConverter as converter
import cv2
import time
import resnet


os.environ["CUDA_VISIBLE_DEVICES"] = "2"
from cifar10 import img_size_cropped, num_channels, num_classes

def train(args):

    # reset the default graph when several runs are executed inside a loop
    tf.reset_default_graph()
    # None- placeholder for different batchsizes
    Inputimage = tf.placeholder(tf.float32, shape=[None, img_size_cropped, img_size_cropped, num_channels],
                       name='Input_Images')

    # build the graph
    Outputlayer = resnet.build_model(Inputimage, num_classes)

    # start training
    training._training(args, Outputlayer)

def predict(args, PB_PATH):

    ''' convert CKPT to PB
    Inputimage = tf.placeholder(tf.float32, shape=[1, args.rows, args.cols, 3],
                       name='Input_Images')

    with tf.name_scope('biometric_model'):
        Outputlayer = resnet.build_model(Inputimage, args.num_categories)

    with tf.Session() as sess:
        sess.graph.as_default()
        sess.run(tf.global_variables_initializer())
        loader = tf.train.Saver()
        loader.restore(sess, '/home/weihan/project/tf_CIFAR_tutorial/CKPT/model_0.906/' + '.ckpt')
        converter.Save_pb(sess, Outputlayer, '/home/weihan/project/tf_CIFAR_tutorial/CKPT/model_0.906/model_0.906')
    '''

    class_dict = {0: 'airplane', 1: 'automobile', 2: 'bird', 3: 'cat', 4: 'deer', 5: 'dog', 6: 'frog', 7: 'horse', 8: 'ship', 9: 'truck'}

    accuracy = 0
    false_list = []
    DIR_path = './predict_data/'
    # 0: airplane, 1: automobile, 2: bird, 3: cat, 4: deer, 5: dog, 6: frog, 7: horse, 8: ship, 9: truck
    category = 1


    with tf.Session() as sess:
        graph_def = converter.get_graphDef_from_FrozenPB(PB_PATH)
        node_list = converter.get_nodelist_from_pb(PB_PATH)
        input_list = converter.get_input_node_list(node_list)
        OutNode = converter.get_last_node_from_nodelist(node_list)
        nodeNameList = []
        nodeNameList.extend(input_list)
        nodeNameList.append(OutNode)
        tensorList = tf.import_graph_def(graph_def, return_elements=nodeNameList)
        outTensor = tensorList[len(tensorList) - 1]

        sess.graph.as_default()
        sess.run(tf.global_variables_initializer())

        for imgName in os.listdir(DIR_path):

            image = cv2.imread(DIR_path + imgName)
            b, g, r = cv2.split(image)
            image = cv2.merge([r, g, b])
            image = image[4:28, 4:28]

            image = np.float32(image) / 255.0
            image = np.expand_dims(image, axis=0)

            tensorDict = converter.get_dict_from_input_tensorList(tensorList, [image])

            start_time = time.time()
            predict_out = sess.run(outTensor, feed_dict=tensorDict)
            duration = time.time() - start_time
            duration = round(duration, 3)

            print(imgName)
            print('duration(s): ', duration)
            result = np.argmax(np.array(predict_out))

            if result == category:
                accuracy = accuracy + 1
            else:
                false_list.append(imgName)

    accuracy = accuracy / len(os.listdir(DIR_path))
    print('accuracy: ', accuracy)
    print('class: ', class_dict[result])
    for list in false_list:
        print(list)


def main():

    parser = argParser()
    args = parser.parse_args()

    ''' training step '''
    #train(args)

    ''' predicting step '''
    predict(args, '/home/weihan/project/tf_CIFAR_tutorial/CKPT/model_0.906/model_0.906_1250_frozen.pb')





if __name__ == '__main__':
    main()