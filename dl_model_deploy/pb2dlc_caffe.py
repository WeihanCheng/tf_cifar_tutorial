import tensorflow as tf
import numpy as np
import os
import subprocess
import shutil
import hashlib
import argparse
import sys
import glob

from subprocess import call

FLAGS = None

def get_FileSize(filePath):
    # filePath = unicode(filePath,'utf8')
    fsize = os.path.getsize(filePath)
    fsize = fsize/float(1024*1024)
    return round(fsize,2)

def convert_to_dlc(caffe_txt, caffe_bin, dlc):
    log_file = caffe_txt.split(".")[0] + "_node_log.txt"
    print("")
    print("==========================Model Configure==========================")
    print("current path: " + os.getcwd())
    print("caffe_txt: " + caffe_txt)
    print("caffe_bin: " + caffe_bin)
    print("dlc: " + dlc)
    print("log_file: " + log_file)
    print("==========================Model Configure==========================")
    print("")
    
    if os.path.isfile(dlc):
        os.remove(dlc)

    cmd = ['snpe-caffe-to-dlc',
           '--caffe_txt', caffe_txt,
           '--caffe_bin', caffe_bin,
           '--dlc', dlc]
    subprocess.call(cmd)
    
    if os.path.isfile(dlc):
        print(".dlc generate pass ^_______^")
    else:
        print(".dlc generate fail Q^Q")
        
    # print(log_file)
    command = 'snpe-dlc-info -i INPUT_DLC --input_dlc %s > %s' % (dlc, log_file)
    # print(command)
    subprocess.call(command, shell=True)
    
    
    caffe_bin_size = get_FileSize(caffe_bin)
    dlc_size = get_FileSize(dlc)
    
    print("caffe_bin_size: ", caffe_bin_size, " MB")
    print("dlc_size: ", dlc_size, " MB")
    if(caffe_bin_size/dlc_size > 2):
        print(".dlc maybe wrong Q^Q")
    else:
        print(".dlc seem good ^_______^")

def main():
    
    convert_to_dlc(FLAGS.caffe_txt, FLAGS.caffe_bin, FLAGS.dlc)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--caffe_txt',
        type=str,
        required=False,
        help='caffe_txt',
        default = "/home/fang/snpe-sdk/models/face/ResNet50/ResNet_50_MS.prototxt"
    )
    parser.add_argument(
        '--caffe_bin',
        type=str,
        required=False,
        help='caffe_bin',
        default = "/home/fang/snpe-sdk/models/face/ResNet50/ResNet50_rand.caffemodel"
    )
    
    parser.add_argument(
        '--dlc',
        type=str,
        required=False,
        help='Path to dlc output file: must be absolute path',
        default = "/home/fang/snpe-sdk/models/face/ResNet50/ResNet_50_MS.dlc"
    )
    FLAGS = parser.parse_args()
    main()