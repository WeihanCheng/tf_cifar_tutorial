import tensorflow as tf
import numpy as np
import os
import subprocess
import shutil
import hashlib
import argparse
import sys
import glob

import TF_GraphConverter as TFconverter

FLAGS = None

def get_FileSize(filePath):
    # filePath = unicode(filePath,'utf8')
    fsize = os.path.getsize(filePath)
    fsize = fsize/float(1024*1024)
    return round(fsize,2)

def get_dlc_out_node(dlc_dir):
    sStr2 = "out_node="
    sStr3 = "["
    sStr4 = "]"
    
    # print(dlc_dir)
    file = open(dlc_dir, "r", encoding="ascii", errors="surrogateescape")
    tmp = file.read()
    
    index_1 = tmp.find(sStr2)
    if index_1 != -1:
        index_2 = tmp.find(sStr3, index_1)
        index_3 = tmp.find(sStr4, index_2)
        out_node = tmp[index_2+2:index_3-1]
    else:
        out_node = "error"
    
    return out_node

def convert_to_dlc_TF_recursive(pd_dir, dlc_dir, input_node = None, input_shape = None, output_node = None):
    last_index = -1
    node_list = TFconverter.get_nodelist_from_pb(pd_dir)
    node_list_acceptable = get_acceptable_node_list(node_list)
    cnt = len(node_list_acceptable)
    print(cnt, " node")
    last_index = cnt-1
    
    if output_node is None:
        print("use last node")
        out_node = node_list_acceptable[last_index].name
        print(out_node)
    else:
        for index in range(last_index, -1, -1):
            print(node_list_acceptable[index].name)
            if node_list_acceptable[index].name == output_node:
                last_index = index
                break
        if last_index <0:
            print(output_node, " doesn't exsit")
            return
        out_node = output_node
        
    if input_node is None or input_shape is None:
        print("input node is None, auto detect input node...")
        input_node = ""
        input_shape = ""
        input_list = TFconverter.get_input_node_list(node_list)
        for i in range(0, len(input_list), 2):
            print(input_list[i])
            print(input_list[i+1])
            input_node = input_node + input_list[i].replace(":0", "")
            input_shape = input_shape + input_list[i+1]
            if i+2 == len(input_list):
                print("This is last node~~~")
            else:
                input_node = input_node + ":"
                input_shape = input_shape + ":"
        print("input_node: ", input_node)
        print("input_shape: ", input_shape)
        
    # return 1 # test by fang


    Pass = convert_to_dlc_TF(pd_dir, dlc_dir, input_node, input_shape, out_node)
    
    result_msg = ""


    if Pass == False:
        tmp_out_node_name = node_list_acceptable[last_index].name
        all_fail = True
        for index in range(last_index-1, -1, -1):
            print(index, node_list_acceptable[index].name)
            Pass_tmp = convert_to_dlc_TF(pd_dir, dlc_dir, input_node, input_shape,
                                                node_list_acceptable[index].name)
            if Pass_tmp == True:
                print("Fail node: ", tmp_out_node_name)
                print("Pass node: ", node_list_acceptable[index].name)
                all_fail = False
                result_msg = "Fail node: " + tmp_out_node_name + " Pass node: " + node_list_acceptable[index].name
                break
            tmp_out_node_name = node_list_acceptable[index].name
        if all_fail == True:
            print("All node Fail....")
            result_msg = "All node Fail"
    else:
        print("All node Pass~~")
        result_msg = "Pass"
        
    return result_msg

    
def convert_to_dlc_TF(pd_dir, dlc_dir, input_node, input_shape, output_node = None):
    log_file = dlc_dir.split(".")[0] + "_node_log.txt"


    if output_node is None:
        print("use last node")
        node_list = TFconverter.get_nodelist_from_pb(pd_dir)
        cnt = len(node_list)
        print(cnt, " node")
        out_node = node_list[cnt-1].name
        print(out_node)
    else:
        out_node = output_node

    print("")
    print("==========================Model Configure==========================")
    print("current path: " + os.getcwd())
    print("pb: " + pd_dir)
    print("input node: " + input_node)
    print("input size: " + input_shape)
    print("output node: " + out_node)
    print("dlc: " + dlc_dir)
    print("log_file: " + log_file)
    print("==========================Model Configure==========================")
    print("")
    
    if os.path.isfile(dlc_dir):
        os.remove(dlc_dir)

    input_node_list = input_node.split(":")
    input_shape_list = input_shape.split(":")
    
    if(len(input_node_list)==1 and len(input_shape_list)==1):
        print("only 1 input")
    
        # ex: input_node='img_placeholder' input_shape='128,128,3' output_node='efc1'
        cmd = ['snpe-tensorflow-to-dlc', 
        '--graph', pd_dir, 
        '--input_dim', input_node, input_shape, 
        '--out_node', out_node,
        '--dlc', dlc_dir
        ,'--allow_unconsumed_nodes'
        # , '--verbose'
        ]
        subprocess.call(cmd)
    else:
        if(len(input_node_list)==len(input_shape_list)):
            print("multiple input")
            command = 'snpe-tensorflow-to-dlc --graph %s --out_node %s --dlc %s --allow_unconsumed_nodes' % (pd_dir, out_node, dlc_dir)
            for node,size in zip(input_node_list, input_shape_list):
                input_dim = ' --input_dim %s %s' % (node, size)
                command = command + input_dim
            subprocess.call(command, shell=True)
            
        else:
            print("list size mismatch")
            return False
    
    if os.path.isfile(dlc_dir):
        print(".dlc generate pass")
    else:
        print(".dlc generate fail Q^Q")
        return False
        
    command = 'snpe-dlc-info -i INPUT_DLC --input_dlc %s > %s' % (dlc_dir, log_file)
    subprocess.call(command, shell=True)
    
    pb_size = get_FileSize(pd_dir)
    dlc_size = get_FileSize(dlc_dir)
    
    print("pb_size: ", pb_size, " MB")
    print("dlc_size: ", dlc_size, " MB")
    dlc_out_node = get_dlc_out_node(log_file) # log_file
    if dlc_out_node==out_node:
        print(".dlc out node match  ^_______^")
        return True
    else:
        print(".dlc maybe wrong Q^Q")
        return False

def acceptable_out_node(node):
    if node.op == "Placeholder":
        return False
    if node.op == "Const":
        return False
    if node.op == "StridedSlice":
        return False
    if node.op == "Mul":
        if node.name.find("Mul") < 0:
            return False
    if node.op == "Pack":
        return False

    return True


def log_node_list(node_list, filename="Output"):
    print(len(node_list), " node")
    with open(filename + ".txt", "w") as text_file:
        cnt = 0
        for node in node_list:
            print(cnt, ": ", node.name, node.op, file=text_file)
            cnt = cnt + 1

def get_acceptable_node_list(node_list):
    outList = []
    cnt = 0
    for node in node_list:
        Pass = acceptable_out_node(node)
        if Pass == True:
            cnt = cnt + 1
            outList.append(node)
    print(len(outList), " acceptabl node")
    return outList


def gen_acceptable_model_all(pd_dir, node_list, input_node, input_shape, out_filename, out_dir):
    pass_node_cnt = 0
    for node in node_list:
        Pass = acceptable_out_node(node)
        if Pass == True:
            pass_node_cnt = pass_node_cnt+1
    print("need to generate ", pass_node_cnt, "model")

    nodel_index = 0
    for node in node_list:
        Pass = acceptable_out_node(node)
        if Pass == True:
            print("Curr op: ", node.name, node.op)
            dlc_dir = out_dir + out_filename + str(nodel_index) + ".dlc"
            convert_to_dlc_TF(pd_dir, dlc_dir, input_node, input_shape, node.name)
            nodel_index = nodel_index+1

def main():
    # just convert
    # convert_to_dlc_TF(FLAGS.graph, FLAGS.output, FLAGS.input_node, FLAGS.input_size, FLAGS.out_node)

    # convert. if fail, log the fail node
    convert_to_dlc_TF_recursive(FLAGS.graph, FLAGS.output, FLAGS.input_node, FLAGS.input_size, FLAGS.out_node)
    
    
    # convert_to_dlc_TF_recursive(FLAGS.graph, FLAGS.output)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--graph',
        type=str,
        required=False,
        help='Path to .pb file: must be absolute path',
        default = "/home/fang/work/dl_model_deploy/curr_pb/Dispnet_test_v2_frozen.pb" # "/home/fang/work/dl_model_deploy/tf_model.pb"
    )
    parser.add_argument(
        '--input_node',
        type=str,
        required=False,
        help='Input node name: see .pbtxt',
        default = None # "img_placeholder"
    )
    
    parser.add_argument(
        '--input_size',
        type=str,
        required=False,
        help='Input shape: see .pbtxt',
        default = None # "256,256,3"
    )
    
    parser.add_argument(
        '--out_node',
        type=str,
        required=False,
        help='Output node name: see .pbtxt',
        default = None # "style/scale_bias/Tanh"
    )
    
    parser.add_argument(
        '--output',
        type=str,
        required=False,
        help='Path to dlc output file: must be absolute path',
        default = "/home/fang/work/dl_model_deploy/tf_model_111111.dlc"
    )
    FLAGS = parser.parse_args()
    print("")
    print("Run SNPE.....")
    main()