
import numpy as np
import tensorflow as tf
# from tensorflow.python.framework import graph_util
# from tensorflow.python.tools import optimize_for_inference_lib
# from tensorflow.python.platform import gfile
# from tensorflow.python.framework import dtypes
# from tensorflow.core.framework import graph_pb2
import argparse
import os
import cv2
import time


# ==============================================================================
# Check if .pb file is acceptable for OpenCV DNN
# ==============================================================================
def CheckTFpbFileByOpenCVDNN(pb_path): 
    try:
        net = cv2.dnn.readNetFromTensorflow(pb_path)
    except Exception as e:
        print ("----------------------------------------------")
        print (pb_path," import Fail")
        print ("----------------------------------------------")
        print('Error message: ' + str(e))
        return -1
    else:
        print (pb_path, " import Pass")
        # inp = np.random.standard_normal([1, 2, 128, 128]).astype(np.float32)
        # net.setInput(inp)
        # out = net.forward()
        return 0

def get_dict_from_input_NodeList(input_NodeList, input_DataList):
    new_dict = {}
    for i in range(len(input_DataList)):
        # print(input_list[i])
        new_dict[input_NodeList[i*2]] = input_DataList[i]
    return new_dict

def get_input_node_list(node_list, noBatch = True):
    inputList = []
    cnt = 0
    remove = "[] \n"
    table = str.maketrans("", "", remove)
        
    for node in node_list:
        if node.op == "Placeholder":
            cnt = cnt + 1
            node_name = node.name + ":0"
            
            shape = node.attr["shape"]
            
            node_size = str(shape.shape.dim).translate(table).replace("size:", "")
            if(noBatch == True):
                node_size = node_size.split(",")
                nodeSize = ''
                for i in range(len(node_size)-1, 0, -1):
                    nodeSize = node_size[i] + "," + nodeSize

            inputList.append(node_name)

    print(len(inputList), " acceptable input node")
    return inputList
        
        
def get_last_node_from_nodelist(node_list):
    cnt = len(node_list)
    # print(cnt, " node")
    last_index = cnt-1
    last_node = node_list[last_index].name + ":0"
    return last_node
        
def PredictByFrozenPB_1(pb_path, InputData, InputNode, OutNode, log_time = True, resetGraph = True):      
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data)
        
    with tf.Session() as sess:
        sess.graph.as_default()
        img_placeholder = tf.placeholder(tf.float32, shape=InputData.shape, name='Placeholder')
        output = tf.import_graph_def(graph_def, input_map={InputNode: img_placeholder}, return_elements=[OutNode])
        start_time = time.time()
        out = sess.run(output, feed_dict={InputNode: InputData})
        duration = time.time() - start_time
        duration = round(duration, 3)
        if log_time == True:
            print ("frozen model predict time: " + str(duration) + " sec")
    
    if resetGraph == True:
        # need to reset default graph for following function, very important !!!!
        from tensorflow.python.framework import ops
        ops.reset_default_graph()
    
    return out

def PredictByFrozenPB(pb_path, InputData, InputNode, OutNode, log_time = True, resetGraph = True):      
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data)
        
    with tf.Session() as sess:
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
        start_time = time.time()
        out = sess.run(OutNode, feed_dict={InputNode: InputData})
        duration = time.time() - start_time
        duration = round(duration, 3)
        if log_time == True:
            print ("frozen model predict time: " + str(duration) + " sec")
    
    if resetGraph == True:
        # need to reset default graph for following function, very important !!!!
        from tensorflow.python.framework import ops
        ops.reset_default_graph()
    
    return out
    
def get_graphDef_from_FrozenPB(pb_path):      
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data) 
    return graph_def
    


def SetGraphByFrozenpb(pb_path, InputNode, InputData, OutNode):
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data)
        
    # img_placeholder = tf.placeholder(tf.float32, shape=InputData.shape, name='Placeholder')
    img_placeholder = tf.placeholder(tf.float32, shape=InputData.shape)
    output = tf.import_graph_def(graph_def, input_map={InputNode: img_placeholder}, return_elements=[OutNode])
    return output
    
def SetGraphByFrozenpb_List(pb_path, InputNodeList, InputDataList, OutNode):
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data)

    placeholder_list = []
    for i in range(len(InputDataList)):
        name = InputNodeList[i*2]
        name = name.split(':', 1)[0]
        img_placeholder = tf.placeholder(tf.float32, shape=InputDataList[i].shape, name=name)
        placeholder_list.append(img_placeholder)

    # input_map_dict = {InputNodeList[0]: placeholder_list[0]}
    output = tf.import_graph_def(graph_def, return_elements=[OutNode])
    return output


def PredictByFrozenPB_dict(pb_path, InputDict, OutNode):
    graph_def = tf.GraphDef()
    with tf.gfile.Open(pb_path, "rb") as f:
        data = f.read()
        graph_def.ParseFromString(data)

    with tf.Session() as sess:
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
        out = sess.run(sess.graph.get_tensor_by_name(OutNode), feed_dict=InputDict)

    # need to reset default graph for following function, very important !!!!
    from tensorflow.python.framework import ops
    ops.reset_default_graph()

    return out

        
def Save_Nullpb(net, pbtxtName):
    # Set up TF session and initialize variables. 
    config = tf.ConfigProto()
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()
    sess.run(init)
    
    Save_pb(sess, net, pbtxtName)

        
# ==============================================================================
# Save only .pbtxt
# ==============================================================================
def Save_pbtxt(net, pbtxtName):
    output_node_names = net.name
    output_node_names =output_node_names.split(":")
    output_node_names = output_node_names[0]

    input_graph_def = net.graph.as_graph_def()
    tf.train.write_graph(input_graph_def, './',pbtxtName+"_Raw.pbtxt")
    print("pbtxt file generated ^____^")
    
    print("Start log all op...")
    for op in net.graph.get_operations():
        print(op.name, op.values())

def Save_lite(sess, input_tensors, net, pbName="TFLite_converted_model", clear_devices = True):
    # manually put back imported modules for tf lite
    import tempfile
    import subprocess
    tf.contrib.lite.tempfile = tempfile
    tf.contrib.lite.subprocess = subprocess

    print("----")
    print(net.name)
    print("----")
    
    output_node_names = net.name
    output_node_names = output_node_names.split(":")
    output_node_names = output_node_names[0]
    input_graph_def = net.graph.as_graph_def()
    
    # Remove all the explicit device specifications for this node. This helps to
    # make the graph more portable.
    if clear_devices:
        for node in input_graph_def.node:
            node.device = ""
    
    
    frozen_graphdef = tf.graph_util.convert_variables_to_constants( 
        sess,
        input_graph_def,
        output_node_names.split(",")
    )

    output_graph_def = tf.graph_util.remove_training_nodes(frozen_graphdef)


    out_tensors = [net]

    tflite_model = tf.contrib.lite.toco_convert(output_graph_def, [input_tensors], out_tensors)
    
    open(pbName+".tflite", "wb").write(tflite_model)        
        
# ==============================================================================
# Save .pb and log its .pbtxt with
# (1) Remove all the explicit device specifications
# (2) Convert variables to constants
# (3) Remove training nodes
# ==============================================================================
def Save_pb(sess, net, pbName, clear_devices = True):
    output_node_names = net.name
    output_node_names = output_node_names.split(":")
    output_node_names = output_node_names[0]
    input_graph_def = net.graph.as_graph_def()
    
    # Remove all the explicit device specifications for this node. This helps to
    # make the graph more portable.
    if clear_devices:
        for node in input_graph_def.node:
            node.device = ""
    
    
    output_graph_def = tf.graph_util.convert_variables_to_constants( 
        sess,
        input_graph_def,
        output_node_names.split(",")
    )
    
    output_graph_def = tf.graph_util.remove_training_nodes(output_graph_def)
    tf.train.write_graph(output_graph_def, './', pbName+"_MobileDeploy.pbtxt")
    print("pbtxt file generated ^____^")
    
    with tf.gfile.GFile(pbName+"_frozen.pb", "wb") as f: 
        f.write(output_graph_def.SerializeToString())
    print("pb file generated ^____^")
    print("%d ops in the final graph." % len(output_graph_def.node))
  
# ==============================================================================
# Optimize .pb with
# (1) Removing training-only operations like checkpoint saving.
# (2) Stripping out parts of the graph that are never reached.
# (3) Removing debug operations like CheckNumerics.
# (4) Folding batch normalization ops into the pre-calculated weights.
# (5) Fusing common operations into unified versions.
# ==============================================================================  
def Convert_to_Opt_pb(FrozenName, OptFrozenName, input_names, output_names):
    from tensorflow.python.tools import optimize_for_inference_lib

    input_names = input_names.split(":")[0]
    output_names = output_names.split(":")[0]


    if not tf.gfile.Exists(FrozenName):
        print("Input graph file '" + FrozenName + "' does not exist!")
        return -1
    print("start to opt Frozen pb file...")

    input_graph_def = tf.GraphDef()
    with tf.gfile.Open(FrozenName, "rb") as f:
        data = f.read()
        input_graph_def.ParseFromString(data)
 

    output_graph_def = optimize_for_inference_lib.optimize_for_inference(
        input_graph_def,
        input_names.split(","),
        output_names.split(","), tf.float32.as_datatype_enum)

    OutFile = tf.gfile.FastGFile(OptFrozenName, "w")
    OutFile.write(output_graph_def.SerializeToString())
    return 0
    

    
def pb2coreml_Simple(pbName, coremlName, input_names, output_names, input_shape = None):
# ==============================================================================
    # _convert_pb_to_mlmodel(tf_model_path,
                           # mlmodel_path,
                           # output_feature_names,
                           # input_name_shape_dict=None,
                           # image_input_names=None,
                           # is_bgr=False,
                           # red_bias=0.0,
                           # green_bias=0.0,
                           # blue_bias=0.0,
                           # gray_bias=0.0,
                           # image_scale=1.0,
                           # class_labels=None,
                           # predicted_feature_name=None,
                           # predicted_probabilities_output='')
# ==============================================================================
    # ex:
        # mlConvert.convert(
            # tf_model_path = 'fused_RD_Net.pb',
            # mlmodel_path = 'RD_Net_test.mlmodel',
            # output_feature_names = ['seq_4/Conv/BiasAdd:0'],
            # input_name_shape_dict = {'Placeholder:0' : [1, 240, 320, 3]})                
# ==============================================================================
    import tfcoreml as mlConvert
    mlConvert.convert(
        tf_model_path = pbName,
        mlmodel_path = coremlName,
        output_feature_names = [output_names],
        input_name_shape_dict = {input_names : input_shape})

# ==============================================================================
# For Low Level Node
# ==============================================================================
def get_nodelist_from_pb(FrozenName):
    if not tf.gfile.Exists(FrozenName):
        print("Input graph file '" + FrozenName + "' does not exist!")
        return -1

    input_graph_def = tf.GraphDef()
    with tf.gfile.Open(FrozenName, "rb") as f:
        data = f.read()
        input_graph_def.ParseFromString(data)

    # test_snpw_node_log(input_graph_def, inputs, out_nodes_names) # for test

    node_list = input_graph_def.node
    print(len(node_list), " node in ", FrozenName)
    return node_list

# ==============================================================================
# For High Level OPS
# ==============================================================================
def get_all_ops_from_pb(pd_dir):
    if not gfile.Exists(pd_dir):
        print("Input graph file '" + pd_dir + "' does not exist!")
        return -1

    input_graph_def = tf.GraphDef()
    with gfile.Open(pd_dir, "rb") as f:
        data = f.read()
        input_graph_def.ParseFromString(data)

    # list = tf.contrib.util.stripped_op_list_for_graph(input_graph_def)
    tf_op_list = tf.contrib.util.ops_used_by_graph_def(input_graph_def)

    return tf_op_list

# ==============================================================================
# For High Level OPS
# ==============================================================================
def merge_list(first_list, second_list):
    merge = first_list + list(set(second_list) - set(first_list))
    return merge

# ==============================================================================
# For High Level OPS
# ==============================================================================
def export_oplist_to_txt(tf_op_list, filename = 'test.txt'):
    text_file = open(filename, 'w')
    for item in tf_op_list:
        text_file.write("%s\n" % item)
    text_file.close()

# ==============================================================================
# For High Level OPS
# ==============================================================================
def import_oplist_from_txt(filename='test.txt'):
    text_file = open('test.txt', 'r')
    tf_op_list = [line.strip() for line in text_file.readlines()]
    print(tf_op_list)
    text_file.close()
    return tf_op_list
    
def get_dict_from_input_tensorList(input_tensorList, input_DataList):
    new_dict = {}
    for i in range(len(input_DataList)):
        new_dict[input_tensorList[i]] = input_DataList[i]
    return new_dict






