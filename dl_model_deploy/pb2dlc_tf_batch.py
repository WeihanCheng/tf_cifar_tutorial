import tensorflow as tf
import numpy as np
import os
import subprocess
import shutil
import hashlib
import argparse
import sys
import glob

from os import listdir
from os.path import isfile, join

import pb2dlc_tf as npe_tf
import TF_GraphConverter as TFconverter

FLAGS = None

def main():
    CurrPath = FLAGS.pb_path

    onlyfiles = [ f for f in listdir(CurrPath) if isfile(join(CurrPath,f)) ]
    
    pbName = []
    for file in onlyfiles:
        SubName = os.path.splitext(file)[-1]
        if(SubName=='.pb'):
            # print(file)
            print(join(CurrPath,file))
            pbName.append(join(CurrPath,file))
            
    
    fo = open("convert_log.txt", "w")
    for pbfile in pbName:
        dlc_file = pbfile.split(".")[0] + "_SNPE.dlc"
        print(dlc_file)
        result_msg = npe_tf.convert_to_dlc_TF_recursive(pbfile, dlc_file)
        str = pbfile + ": " + result_msg + "\n"
        fo.write(str)
        
    fo.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--pb_path',
        type=str,
        required=False,
        help='Path to .pb file: must be absolute path',
        default = "/home/fang/work/dl_model_deploy/curr_pb/"
    )

    FLAGS = parser.parse_args()
    print("")
    print("Run SNPE.....")
    main()