# deep learning model deploy module

## TF_GraphConverter.py
some function for tensorflow operation

## pb2dlc_caffe.py
convert caffe model to .dlc

ex:
```
python3 pb2dlc_caffe.py --caffe_txt caffe_model.prototxt --caffe_bin caffe_model.caffemodel --dlc caffe_model.dlc
```


## pb2dlc_tf.py
convert .pb to .dlc

ex:
```
python3 pb2dlc_tf.py --graph tf_model.pb --input_node img_placeholder --input_size 256,256,3 --out_node style/scale_bias/Tanh --output tf_model.dlc
```


## pb2dlc_tf_batch.py
convert all .pb in assigned folder to .dlc

ex:
```
python3 pb2dlc_tf_batch.py --pb_path /home/fang/work/dl_model_deploy/curr_pb/
```