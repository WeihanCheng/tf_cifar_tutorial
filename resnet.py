import tensorflow as tf


def resnet_block(input, out_channel, kernal_seze):
    shortcut = input

    bn_1 = tf.layers.batch_normalization(input)
    relu_1 = tf.nn.relu(bn_1)
    conv_1 = tf.layers.conv2d(relu_1, out_channel, [kernal_seze, kernal_seze], strides=1, padding='SAME')

    bn_2 = tf.layers.batch_normalization(conv_1)
    relu_2 = tf.nn.relu(bn_2)
    conv_2 = tf.layers.conv2d(relu_2, out_channel, [kernal_seze, kernal_seze], strides=1, padding='SAME')


    resnet_out = tf.add(conv_2, shortcut)

    return resnet_out


def build_model(images, num_classes=2):

    # conv1
    with tf.variable_scope('conv1'):
        with tf.variable_scope('1'):
            conv1 = tf.layers.conv2d(images, 32, [3, 3], strides=1, padding='SAME')
        with tf.variable_scope('bn_1'):
            bn_1 = tf.layers.batch_normalization(conv1)
        with tf.variable_scope('relu_1'):
            relu_1 = tf.nn.relu(bn_1)

    # conv2
    with tf.variable_scope('conv2'):
        with tf.variable_scope('0'):
            conv2_x = tf.layers.conv2d(relu_1, 64, [1, 1], padding='SAME')
        with tf.variable_scope('1'):
            conv2_x = resnet_block(conv2_x, 64, 3)
        with tf.variable_scope('2'):
            conv2_x = resnet_block(conv2_x, 64, 3)
        with tf.variable_scope('3'):
            conv2_x = tf.layers.max_pooling2d(conv2_x, [3, 3], strides=2)

    # conv3
    with tf.variable_scope('conv3'):
        with tf.variable_scope('0'):
            conv3_x = tf.layers.conv2d(conv2_x, 128, [1, 1], padding='SAME')  # to fit channel
        with tf.variable_scope('1'):
            conv3_x = resnet_block(conv3_x, 128, 3)
        with tf.variable_scope('2'):
            conv3_x = resnet_block(conv3_x, 128, 3)
        with tf.variable_scope('3'):
            conv3_x = tf.layers.max_pooling2d(conv3_x, [3, 3], strides=2, padding='SAME')

    # conv4
    with tf.variable_scope('conv4'):
        with tf.variable_scope('0'):
            conv4_x = tf.layers.conv2d(conv3_x, 256, [1, 1], padding='SAME')  # to fit channel
        with tf.variable_scope('1'):
            conv4_x = resnet_block(conv4_x, 256, 3)
        with tf.variable_scope('2'):
            conv4_x = resnet_block(conv4_x, 256, 3)
        with tf.variable_scope('3'):
            conv4_x = tf.layers.max_pooling2d(conv4_x, [3, 3], strides=2, padding='SAME')

    # conv5
    with tf.variable_scope('conv5'):
        with tf.variable_scope('0'):
            conv5_x = tf.layers.conv2d(conv4_x, 128, [3, 3], strides=1, padding='SAME')
        with tf.variable_scope('bn_0'):
            bn_5 = tf.layers.batch_normalization(conv5_x)
        with tf.variable_scope('relu_0'):
            relu_5 = tf.nn.relu(bn_5)
        with tf.variable_scope('1'):
            conv5_x = tf.layers.conv2d(relu_5, 64, [3, 3], strides=1, padding='SAME')
        with tf.variable_scope('bn_1'):
            bn_5 = tf.layers.batch_normalization(conv5_x)
        with tf.variable_scope('relu_1'):
            relu_5 = tf.nn.relu(bn_5)
        with tf.variable_scope('2'):
            conv5_x = tf.layers.conv2d(relu_5, 32, [3, 3], strides=1, padding='SAME')
        with tf.variable_scope('bn_2'):
            bn_5 = tf.layers.batch_normalization(conv5_x)
        with tf.variable_scope('relu_2'):
            relu_5 = tf.nn.relu(bn_5)
        with tf.variable_scope('3'):
            conv5_x = tf.layers.conv2d(relu_5, num_classes, [3, 3], strides=1, padding='SAME')


    with tf.variable_scope('global_avg_pooling'):
        with tf.variable_scope('0'):
            conv_glob_avg_pool = tf.layers.average_pooling2d(conv5_x, [conv5_x.shape[1], conv5_x.shape[2]], strides=1)

    with tf.variable_scope('reshape'):
        OutLayer = tf.reshape(conv_glob_avg_pool, [-1, conv_glob_avg_pool.shape[3]])



    return OutLayer