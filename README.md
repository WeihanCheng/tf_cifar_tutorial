# tf_CIFAR_tutorial

Dataset = CIFAR-10
Categories = 10
Training set = 50,000    Testing set = 10,000
Network = ResNet
Accuracy = 90.6 %
Including: 
	1. Global avg pooling instead of fc
	2. Resnet template with recommended order
	3. Augmentation
	4. Dynamic learning rate
